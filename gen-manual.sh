#!/bin/bash

# This script will create a local copy of the ACL2 documentation.
#
# It may be easier to just download a copy from the ACL2 Web page,
# since this can take a while and the build process is often updated,
# which means this script may be out of date at times.
# 
# Follow the instructions for cert.pl from the ACL2 documentation
# topic cert.pl. In particular, see the preliminaries subtopic
# (BUILD____PRELIMINARIES).
#
# Make sure you set the environment variable ACL2_SYSTEM_BOOKS as per
# the cert.pl instructions and make sure that you configured your
# $PATH so that running acl2 will invoke ACL2.
#
# Next, set the environment ACL2S_NUM_JOBS to be the number of jobs
# you want to run in parallel. This allows us to build books in
# parallel. A good rule of thumb is set this to the number of cores on
# your machine.

if [[ -z "${ACL2_SYSTEM_BOOKS}" ]]; then
    echo "Error: the environement variable ACL2_SYSTEM_BOOKS must be defined!"
    echo "Set ACL2_SYSTEM_BOOKS as per the cert.pl instructions"
    exit 1
fi

if [[ -z "${ACL2S_NUM_JOBS}" ]]; then
    echo "Error: the environement variable ACL2S_NUM_JOBS must be defined!"
    echo "Set ACL2S_NUM_JOBS to be the number of jobs you want to run in parallel."
    echo "A good rule of thumb is to set this to the number of cores on your machine."
    exit 1
fi

cd $ACL2_SYSTEM_BOOKS
if make -j "$ACL2S_NUM_JOBS" manual ACL2="$ACL2_SYSTEM_BOOKS"/../saved_acl2; then 
    echo The ACL2 manual was successfully built.
    echo See "$ACL2_SYSTEM_BOOKS/doc/manual/index.html"
    cd -
    exit 0
else
    echo "The build process for the manual failed"
    exit 1
fi
